﻿#include <iostream>
//#include <time.h>

int const N = 20;

/*  Написать функцию, которая в зависимости от своих параметров 
    печатает в консоль либо четные, либо нечетные числа от 0 до N 
    (N тоже сделать параметром функции).
*/

// 1-ый Вариант решения задания громоздкий и долгий
void EvenNumber(bool even, int number)
{
    if (even)
    {
        std::cout << "II.1 even numbers:\n";
        for (int i = 0; i <= N; i++)
        {
            if (!(i % 2))
               std::cout << i << std::endl;
        }
    }
    else if (!even)
    {
        std::cout << "II.1 not even numbers:\n";
        for (int i = 0; i <= N; i++)
        {
            if (i % 2)
               std::cout << i << std::endl;
        }
    }

}

//2-ой Вариант, самый компактный, но практически
//не даёт прирост в скорости работы
void EvenNumber2(bool even, int number)
{
    if (even) std::cout << "II.2 even numbers:\n";
    if (!even) std::cout << "II.2 not even numbers:\n";
    for (int i = 0; i <= N; i++)
    {
        if ((even && !(i % 2)) || (!even && (i % 2)))
            std::cout << i << std::endl;
    }
}

//3-ий Вариант, работает в Два раза быстрее, 
//но не компактный
void EvenNumber3(bool even, int number)
{
    if (even)
    {
        std::cout << "II.3 even numbers:\n";
        for (int i = 0; i <= N; i+=2)
        {
            std::cout << i << std::endl;
        }
    }
    else if (!even)
    {
        std::cout << "II.3 not even numbers:\n";
        for (int i = 1; i <= N; i += 2)
        {
            std::cout << i << std::endl;
        }
    }
}

int main()
{
    /*  Используя цикл и условия вывести в консоль все четные числа 
         от 0 до N (N задавать константой в начале программы).
    */

    std::cout << "I even numbers:\n";
    for (int i = 0; i <= N; i++)
    {
        if (!(i%2))
        std::cout << i << std::endl;
    }

    //Если первый параметр True, то выдаёт четные числа
    //При False, нечетные числа соответственно
    EvenNumber(true, N);
    EvenNumber(false, N);
    EvenNumber2(true, N);
    EvenNumber2(false, N);
    EvenNumber3(true, N);
    EvenNumber3(false, N);
}

/*

// Так выполнял проверку скорости работы, 
// более подробно смотреть в Git'е в 
// Branch'е под названием "Time Test" 

start = clock();
EvenNumber3(false, N);
end = clock();
seconds = (double)(end - start) / CLOCKS_PER_SEC;
std::cout << "3 time: " << seconds << "\n";
*/
